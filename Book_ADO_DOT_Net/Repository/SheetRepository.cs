﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Book_ADO_DOT_Net.Repository
{
    interface ISheetRepository
    {
        List<Sheet> GetAll();
        Sheet GetById(int id);
        void Add(Sheet sheet);
        void Delete(int id);
        void Update(Sheet sheet);
        void ChangeCurrency(int sourceSheetId, int destinationSheetId, int amount);
    }

    class SheetRepository : ISheetRepository
    {
        private static string connectString = @"Data Source=DESKTOP-IOHQV8L;Initial Catalog=DB_for_ADO;User ID=admin;Password=admin;";
        public void Add(Sheet sheet)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = @$"INSERT INTO Sheet([UserId],[Amount],[TypeOfAmount]) VALUES ('{sheet.UserId}','{sheet.Amount}',{(int)sheet.TypeOfAmount})
                        SELECT SCOPE_IDENTITY()";
                using (var dataReader = com.ExecuteReader())
                {
                    dataReader.Read();
                    sheet.Id = decimal.ToInt32((decimal)dataReader[0]);
                }
            }
        }

        public void ChangeCurrency(int sourceSheetId, int destinationSheetId, int amountRub)
        {// передаём счёт с которого списываем, счёт на который записываем, сумма в рублях
            using (var connecting = new SqlConnection(connectString))// коннектимся к нашей БД, создаём подключение
            {
                connecting.Open();//открываем подключение
                var transaction = connecting.BeginTransaction();//создаём транзакцию
                try
                {
                    var com1 = connecting.CreateCommand();//создаём комманду
                    com1.Transaction = transaction;//связываем нашу комманду с нашей транзакцией
                    com1.CommandText = @$"Select [Amount] FROM Sheet 
                                    where [ID] = {sourceSheetId}";//запихиваем наш запрос в БД в нашу комманду
                    var oldRub = 0;
                    using (var dataReader = com1.ExecuteReader())//создаём dataReader, он содержит в себе результат com1.ExecuteReader()
                    {
                        dataReader.Read();//читаем результат выполнения комманды
                        oldRub = (int)dataReader[0];
                    }

                    if (oldRub < amountRub)
                    {
                        throw new Exception("Not enofe rubels!");
                    }
                    var newRub = oldRub - amountRub;
                    var com2 = connecting.CreateCommand();
                    com2.Transaction = transaction;
                    com2.CommandText = @$"UPDATE Sheet 
                                    SET [Amount] = '{newRub}'
                                    where ID = {sourceSheetId}";
                    com2.ExecuteNonQuery();

                    var com3 = connecting.CreateCommand();
                    com3.Transaction = transaction;
                    com3.CommandText = @$"Select Amount FROM Sheet 
                                    where ID = {destinationSheetId}";
                    var oldDol = 0;
                    using (var dataReader = com3.ExecuteReader())
                    {
                        dataReader.Read();
                        oldDol = (int)dataReader[0];
                    }

                    var newDol = oldDol + amountRub / 2;

                    var com4 = connecting.CreateCommand();
                    com4.Transaction = transaction;
                    com4.CommandText = @$"UPDATE Sheet 
                                    SET [Amount] = '{newDol}'
                                    where ID = {destinationSheetId}";
                    com4.ExecuteNonQuery();

                    //throw new Exception(); // demo

                    transaction.Commit();//завершаем нашу транзакцию и -разлочиваем БД
                }
                catch (Exception ex)
                {
                    transaction.Rollback();//если произошла ошибка  -тут отменяем все действия из блока try и разлочиваем БД
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public List<Sheet> GetAll()
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = "Select ID, UserId, Amount, TypeOfAmount from Sheet";
                using (var dataReader = com.ExecuteReader())
                {
                    var myList = new List<Sheet>();
                    while (dataReader.Read())
                    {
                        var sheet = new Sheet
                        {
                            Id = (int)dataReader["ID"],
                            UserId = (int)dataReader["UserId"],
                            Amount = (int)dataReader["Amount"],
                            TypeOfAmount = (TypeOfAmount)dataReader["TypeOfAmount"]
                        };
                        myList.Add(sheet);
                    }
                    return myList;
                }
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Sheet GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Sheet sheet)
        {
            throw new NotImplementedException();
        }
    }

    class Sheet
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int Amount { get; set; }
        public TypeOfAmount TypeOfAmount { get; set; }
    }

    enum TypeOfAmount
    {
        Rub = 1,
        Dol = 2
    }
}
