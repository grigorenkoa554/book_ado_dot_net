﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book_ADO_DOT_Net.Repository
{
    interface IAdressRepository
    {
        List<Adress> GetAll();
        Adress GetById(int id);
        void AddAdress(Adress adress);
        void Update(Adress adress);
        void Delete(int id);

    }
    class AdressRepository : IAdressRepository
    {
        public void AddAdress(Adress adress)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Adress> GetAll()
        {
            throw new NotImplementedException();
        }

        public Adress GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Adress adress)
        {
            throw new NotImplementedException();
        }
    }

    class Adress
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
    }
}
