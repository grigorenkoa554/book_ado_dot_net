﻿using Book_ADO_DOT_Net.Repository;
using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Principal;

namespace Book_ADO_DOT_Net
{
    class Program
    {
        static void Main(string[] args)
        {
            var sheetRepository = new SheetRepository();

            sheetRepository.ChangeCurrency(1, 2, 100);
            return;

            var repos = new UserRepository();
            var user = new User { FirstName = "Solnishko", LastName = "Horoshee", Age = 100000 };
            repos.AddUser(user);
            var sheetRub = new Sheet { UserId = user.Id, Amount = 1000, TypeOfAmount = TypeOfAmount.Rub };
            sheetRepository.Add(sheetRub);
            var sheetDol = new Sheet { UserId = user.Id, Amount = 0, TypeOfAmount = TypeOfAmount.Dol };
            sheetRepository.Add(sheetDol);
            foreach (var item in sheetRepository.GetAll())
            {
                Console.WriteLine($"Id: {item.Id}, UserId: {item.UserId}, Amount: {item.Amount}, TypeOfAmount: {item.TypeOfAmount}");
            }

            return;
            ShowMyUsers(repos);//  
            Console.WriteLine("Add some user");
            Console.WriteLine($"NewId: {user.Id}");
            ShowMyUsers(repos);//
            Console.WriteLine("Delete some user");
            repos.Delete(2);
            ShowMyUsers(repos);//
            Console.WriteLine("Get by id AND Update some user");
            var userok = repos.GetById(1);
            userok.FirstName = "Alex";
            repos.Update(userok);
            ShowMyUsers(repos);//
        }

        static void ShowMyUsers(UserRepository repos)
        {
            foreach (var item in repos.GetAll())
            {
                Console.WriteLine($"{item.Id}) FirstName = {item.FirstName}, LastName = {item.LastName}, Age = {item.Age}");
            }
            Console.WriteLine("-----------------------------------------------------------------------------------------------");
        }
        static void MyUsers()
        {
            //ShowUser();
            //string connectionString = @"Data Source=DESKTOP-IOHQV8L; Initial Catalog=DbDemoFirst; Integrated Security=True";
            string connectionString = @"Data Source=DESKTOP-IOHQV8L;Initial Catalog=DbDemoFirst;User ID=admin;Password=admin;";
            //13 строка - объявляем строку подключения к базе данных 
            //Data Source=DESKTOP-IOHQV8L - имя движка базы данных
            //Initial Catalog=DbDemoFirst - имя базы данных, с которой мы дем работать
            //Integrated Security=True - тип авторизации при подключении к базе данных
            using (DbConnection connection = new SqlConnection(connectionString))
            //создаём подключение к базе данных
            {
                connection.Open();
                DbCommand command = connection.CreateCommand();//создаём комманду, с которой будем работать
                command.CommandText = "Select email, age, password From Users";// в CommandText вставлям запрос, который будет выполняться

                using (DbDataReader dataReader = command.ExecuteReader())
                //создаём dataReader, который внутри себя содержит результат запроса command.ExecuteReader()
                {
                    Console.WriteLine($"Your data reader object is a: {dataReader.GetType().Name}");//выводим имя dataReader
                    Console.WriteLine("\n***** Current Inventory *****");
                    while (dataReader.Read())
                    {
                        //выводим данные с наших колонок с БД по рядкам
                        Console.WriteLine($"-> email: {dataReader["email"]}; age: {dataReader["age"]}; password: {dataReader["password"]}.");
                        Console.WriteLine($"-> email: {dataReader[0]}; age: {dataReader[1]}; password: {dataReader[2]}.");
                    }
                }
            }
        }
        static void ShowUser()
        {
            Console.WriteLine("Hello World!");
            var currentUser = WindowsIdentity.GetCurrent();
            Console.WriteLine($"IsSystem: {currentUser.IsSystem}");
            Console.WriteLine($"Name: {currentUser.Name}");
            Console.WriteLine($"Name: {currentUser.User.Value}");
            Console.WriteLine($"Name: {currentUser.User.AccountDomainSid}");
            Console.WriteLine($"Name: {currentUser.User.BinaryLength}");
            Console.WriteLine($"AuthenticationType: {currentUser.AuthenticationType}");
            Console.WriteLine($"Actor: {currentUser.Actor}");
            Console.WriteLine("--------------------------");
            foreach (var item in currentUser.UserClaims)
            {
                Console.WriteLine($"{item.Type} - {item.Value}");
            }
            return;
        }
    }
}