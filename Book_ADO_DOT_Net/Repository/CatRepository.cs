﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Book_ADO_DOT_Net.Repository
{
    interface ICatRepository
    {
        List<Cat> GetAll();
        Cat GetById(int id);
        void AddCat(Cat cat);
        void Delete(int id);
        void Update(Cat cat);
    }

    class CatRepository : ICatRepository
    {
        private static string connectString = @"Data Source=DESKTOP-IOHQV8L;Initial Catalog=Cat_for_ADO;User ID=admin;Password=admin;";
        public void AddCat(Cat cat)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = @$"INSERT INTO Cats([Name],[Color],[Age]) VALUES ('{cat.Name}','{cat.Color}',{cat.Age})
                        SELECT SCOPE_IDENTITY()";
                using (var dataReader = com.ExecuteReader())
                {
                    dataReader.Read();
                    cat.Id = decimal.ToInt32((decimal)dataReader[0]);
                }
            }
        }

        public void Delete(int id)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = $"DELETE FROM Cats where ID = {id}";
                com.ExecuteNonQuery();
            }
        }

        public List<Cat> GetAll()
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = "Select Id, Name, Color, Age from Cats";
                using (var dataReader = com.ExecuteReader())
                {
                    var myList = new List<Cat>();
                    while (dataReader.Read())
                    {
                        var cat = new Cat
                        {
                            Id = (int)dataReader["Id"],
                            Name = (string)dataReader["Name"],
                            Color = (string)dataReader["Color"],
                            Age = (int)dataReader["Age"]
                        };
                        myList.Add(cat);
                    }
                    return myList;
                }
            }
        }

        public Cat GetById(int id)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = $"Select Id, Name, Color, Age from Cats where Id = {id}";
                using (var dataReader = com.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var cat = new Cat
                        {
                            Id = (int)dataReader["Id"],
                            Name = (string)dataReader["Name"],
                            Color = (string)dataReader["Color"],
                            Age = (int)dataReader["Age"]
                        };
                        return cat;
                    }
                    return null;
                }
            }
        }

        public void Update(Cat cat)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = @$"UPDATE Cats 
                                    SET [Name] = '{cat.Name}', [Color] = '{cat.Color}', [Age] = {cat.Age}
                                    where Id = {cat.Id}";
                com.ExecuteNonQuery();
            }
        }
    }

    class Cat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int Age { get; set; }
    }
}
