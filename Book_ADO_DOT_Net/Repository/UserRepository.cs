﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Book_ADO_DOT_Net.Repository
{

    interface IUserRepository
    {
        List<User> GetAll();
        User GetById(int id);
        void AddUser(User user);
        void Delete(int id);
        void Update(User user);
    }

    class UserRepository : IUserRepository
    {
        private static string connectString = @"Data Source=DESKTOP-IOHQV8L;Initial Catalog=DB_for_ADO;User ID=admin;Password=admin;";
        public void AddUser(User user)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = @$"INSERT INTO Users([FirstName],[LastName],[age]) VALUES ('{user.FirstName}','{user.LastName}',{user.Age})
                        SELECT SCOPE_IDENTITY()";
                using (var dataReader = com.ExecuteReader())
                {
                    dataReader.Read();
                    user.Id = decimal.ToInt32((decimal)dataReader[0]);
                }
            }
        }

        public void Delete(int id)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = $"DELETE FROM Users where ID = {id}";
                com.ExecuteNonQuery();
            }
        }

        public List<User> GetAll()
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = "Select ID, FirstName, LastName, Age from Users";
                using (var dataReader = com.ExecuteReader())
                {
                    var myList = new List<User>();
                    while (dataReader.Read())
                    {
                        var user = new User
                        {
                            Id = (int)dataReader["ID"],
                            FirstName = (string)dataReader["FirstName"],
                            LastName = (string)dataReader["LastName"],
                            Age = (int)dataReader["Age"]
                        };
                        myList.Add(user);
                    }
                    return myList;
                }
            }
        }

        public User GetById(int id)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = $"Select ID, FirstName, LastName, Age from Users where ID = {id}";
                using (var dataReader = com.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var user = new User
                        {
                            Id = (int)dataReader["ID"],
                            FirstName = (string)dataReader["FirstName"],
                            LastName = (string)dataReader["LastName"],
                            Age = (int)dataReader["Age"]
                        };
                        return user;
                    }
                    return null;
                }
            }
        }

        public void Update(User user)
        {
            using (var connecting = new SqlConnection(connectString))
            {
                connecting.Open();
                var com = connecting.CreateCommand();
                com.CommandText = @$"UPDATE Users 
                                    SET [FirstName] = '{user.FirstName}', [LastName] = '{user.LastName}', [Age] = {user.Age}
                                    where ID = {user.Id}";
                com.ExecuteNonQuery();
            }
        }
    }

    class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
